﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;
using Utilities.Helpers;
using System;

namespace Utilities
{
    /// <summary>
    /// Used to mark component fields which values are injected by DependencyInjector
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class PrefabInject : PropertyAttribute {}
}