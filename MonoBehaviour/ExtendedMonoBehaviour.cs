﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using Utilities.Helpers;
using System.Linq;
using UnityEditor;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace Utilities
{
    [InitializeOnLoad]
    public abstract class ExtendedMonoBehaviour : MonoBehaviour
    {
        #if UNITY_EDITOR
        private void CacheSelfInjectComponents()
        {
            List<FieldInfo> selfInjectFields = ReflectionHelpers.GetFieldsWithAttribute(GetType(), typeof(SelfInject));
            foreach (FieldInfo field in selfInjectFields)
            {
                field.SetValue(this, GetComponent(field.FieldType));
            }
        }

        private void CachePrefabInjectComponents()
        {
            GameObject nearestPrefabRootObject = PrefabUtility.GetNearestPrefabInstanceRoot(this);

            if(nearestPrefabRootObject == null)
            {
                return;
            }

            List<FieldInfo> injectFields = ReflectionHelpers.GetFieldsWithAttribute(GetType(), typeof(PrefabInject));

            foreach (FieldInfo field in injectFields)
            {
                if(field.FieldType.IsGenericList())
                {
                    Type elementType = field.FieldType.GetGenericArguments()[0];
                    Component[] components = nearestPrefabRootObject.GetComponentsInChildren(elementType, true);
                    IList coomponentsList = ReflectionHelpers.CreateList(elementType, components);
                    field.SetValue(this, coomponentsList);
                }
                else if(field.FieldType.IsArray)
                {
                    Type elementType = field.FieldType.GetElementType();
                    Component[] components = nearestPrefabRootObject.GetComponentsInChildren(elementType, true);
                    var array = Array.CreateInstance(elementType, components.Length);
                    for (int i = 0; i < components.Length; i++)
                    {
                        array.SetValue(components[i], i); 
                    }
                    field.SetValue(this, array);
                }
                else
                {
                    Component component = nearestPrefabRootObject.GetComponentInChildren(field.FieldType, true);
                    field.SetValue(this, component);
                }
            }
        }

        protected virtual void OnValidate()
        {
            CacheSelfInjectComponents();
            CachePrefabInjectComponents();
        }


    }
    #endif
}
