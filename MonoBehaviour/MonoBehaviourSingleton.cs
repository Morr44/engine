﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    // Base class for MonoBehaviour singleton objects
    public class MonoBehaviourSingleton<T> : ExtendedMonoBehaviour where T : Component
    {

        //
        public static T Instance
        {
            get; private set;
        }

        #region UnityEvents
        //
        protected virtual void Awake()
        {
            if(Instance == null)
            {
                Instance = GetComponent<T>();
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }
        }
        #endregion


    }


}