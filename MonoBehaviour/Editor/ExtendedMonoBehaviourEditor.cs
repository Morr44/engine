﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities.Helpers;
using UnityEditor;
using System;
using System.Reflection;

namespace Utilities
{
    [CustomEditor(typeof(ExtendedMonoBehaviour), true)]
    public class ExtendedMonoBehaviourEditor : Editor
    {
        private string[] InjectedPropertiesNames { get; set; }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawInspector(serializedObject);
            serializedObject.ApplyModifiedProperties();
        }

        private static bool DrawInspector(SerializedObject serializedObject, params string[] propertyToExclude)
        {
            EditorGUI.BeginChangeCheck();
            serializedObject.UpdateIfRequiredOrScript();

            SerializedProperty property = serializedObject.GetIterator();
            bool expanded = true;
            while (property.NextVisible(expanded))
            {
                if (property.HasAnyOfAttributes(typeof(SelfInject), typeof(PrefabInject)))
                {
                    continue;
                }
                using (new EditorGUI.DisabledScope("m_Script" == property.propertyPath))
                {
                    string displayedName = NamingHelpers.NicifyBackingFieldName(property.displayName);
                    EditorGUILayout.PropertyField(property, new GUIContent(displayedName), true);
                }
                expanded = false;
            }

            serializedObject.ApplyModifiedProperties();
            return EditorGUI.EndChangeCheck();
        }


    }
}