﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;
using Utilities.Helpers;
using System;
using UnityEditor;

namespace Utilities
{
    /// <summary>
    /// Mark component type field to be cached on Awake
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class SelfInject : PropertyAttribute { }
}