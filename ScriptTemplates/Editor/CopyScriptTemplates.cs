﻿using UnityEngine;
using UnityEditor;
using Utilities.EditorHelpers;

namespace Utilities
{
    /// <summary>
    /// Copies ScriptTemplates folder to the Assets root directory
    /// </summary>
    [InitializeOnLoad]
    public class CopyScriptTemplates
    {
        private const string destinationPath = "Assets\\ScriptTemplates";

        /// <summary>
        /// Copies current folder to the Assets directory and removes CopyScriptTemplates script copy
        /// </summary>
        static CopyScriptTemplates()
        {
            string fileName = typeof(CopyScriptTemplates).Name;
            string currentDirectory = ScriptFileHelpers.GetScriptDirectory(fileName);

            if (AssetDatabase.IsValidFolder(destinationPath) == false)
            {
                Debug.Log($"Copying ScriptTemplates to '{destinationPath}'");
                AssetDatabase.CopyAsset(currentDirectory, destinationPath);
                AssetDatabase.DeleteAsset(destinationPath + "\\" + fileName + ".cs");
            }
        }
    }
}
