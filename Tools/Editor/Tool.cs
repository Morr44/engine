﻿using UnityEditor;
using UnityEngine;
using Utilities.EditorHelpers;
using System.IO;

namespace Utilities.EditorTools
{
    /// <summary>
    /// Base class for custom editor tools
    /// </summary>
    /// <typeparam name="T">Specific tool type (derived from EditorTool)</typeparam>
    public abstract class Tool<T> : ScriptableObject where T : Tool<T>
    {
        private static T scriptableObjectAsset;
        [SerializeField] protected bool enabled;

        //
        protected string ScriptFileDirectory
        {
            get { return ScriptFileHelpers.GetScriptDirectory(GetType().Name); }
        }

        //
        public static T ToolScriptableObject
        {
            get
            {
                if (scriptableObjectAsset == null)
                {
                    string scriptPath = ScriptFileHelpers.GetScriptPath(typeof(T).Name);
                    string assetPath = Path.ChangeExtension(scriptPath, ".asset");
                    scriptableObjectAsset = AssetDatabase.LoadAssetAtPath<T>(assetPath);
                     
                    if (scriptableObjectAsset == null)
                        CreateScriptableObjectAsset();
                        
                }
                return scriptableObjectAsset;
            }
        }


        //
        protected static void CreateScriptableObjectAsset()
        {
            string directory = Path.GetDirectoryName(ScriptFileHelpers.GetScriptPath(typeof(T).Name));
            scriptableObjectAsset = ScriptableObjectHelpers.CreateScriptableObjectAsset<T>(directory);
            AssetDatabase.SaveAssets();
        }

    }
}
