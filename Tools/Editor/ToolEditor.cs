﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Utilities;


namespace Utilities.EditorTools
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class ToolEditor : Editor
    {

        protected const string ToolsMenuPath = "Tools/";
        private SerializedProperty enabledProperty;

        //
        protected void DisplaySettings()
        {
            EditorGUI.BeginChangeCheck();
            serializedObject.Update();

            // Display settings
            DisplayGeneralSettings();
            if (enabledProperty.boolValue) { DisplayToolSpecificSettings(); }
                
            serializedObject.ApplyModifiedProperties();
            if (EditorGUI.EndChangeCheck()) { ApplySettings(); }
        }


        //
        protected virtual void DisplayGeneralSettings()
        {
            EditorGUILayout.LabelField("Settings", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(enabledProperty);
            EditorGUILayout.Space();
        }


        //
        protected abstract void DisplayToolSpecificSettings();


        //
        protected bool DisplayCmpilationCommunicate()
        {
            if (EditorApplication.isCompiling)
            {
                EditorGUILayout.LabelField("Compiling scripts...");
                return true;
            }
            return false;
        }


        //
        protected virtual void ApplySettings()
        {
            EditorUtility.SetDirty(Selection.activeObject);
        }


        #region UnityEvents

        //
        protected virtual void OnEnable()
        {
            enabledProperty = serializedObject.FindProperty("enabled");
        }

        //
        public override void OnInspectorGUI()
        {
            if (DisplayCmpilationCommunicate()) { return; };
            DisplaySettings();
        }

        #endregion

    }
}