﻿using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Utilities.EditorTools
{

    [CustomEditor(typeof(SceneAssetsManager))]
    public class SceneAssetsManagerEditor : ToolEditor
    {

        private SerializedProperty loadInitSceneProperty;
        private string initScenePath;
        private Object initSceneObject;


        //
        protected override void DisplayToolSpecificSettings()
        {
            EditorGUILayout.LabelField("Init scene", EditorStyles.boldLabel);

            
            if(EditorBuildSettings.scenes.Length > 0)
            {
                // Cache scene object
                var initScene = EditorBuildSettings.scenes[0];
                if (initScene.path != initScenePath)
                {
                    initScenePath = initScene.path;
                    initSceneObject = AssetDatabase.LoadAssetAtPath<Object>(initScenePath);
                }

                EditorGUILayout.ObjectField(initSceneObject, typeof(Scene), true);
            }

            EditorGUILayout.PropertyField(loadInitSceneProperty);
        }


        #region MenuItems

        //
        [MenuItem(ToolsMenuPath + "Scene Assets Manager")]
        public static void SelectToolScriptableObject()
        {
            Selection.activeObject = SceneAssetsManager.ToolScriptableObject;
        }

        #endregion


        #region UnityEvents

        protected override void OnEnable()
        {
            base.OnEnable();
            loadInitSceneProperty = serializedObject.FindProperty("loadInitScene");
        }

        #endregion

    }
}