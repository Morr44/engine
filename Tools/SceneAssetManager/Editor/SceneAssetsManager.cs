﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using Utilities.Helpers;
using Utilities.EditorHelpers;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

namespace Utilities.EditorTools
{
    /// <summary>
    /// Generates scene enum for scenes included in the build
    /// Loads init on play in editor mode
    /// </summary>
    [InitializeOnLoad]
    public class SceneAssetsManager : Tool<SceneAssetsManager>
    {
        const string EnumDescription = "Generated scenes enum";
        const string PreviousScenePrefsKey = "SceneAssetManager.PreviousScene";
        [SerializeField] private bool loadInitScene = true;


        //
        static SceneAssetsManager()
        {
            EditorBuildSettings.sceneListChanged += () => { ToolScriptableObject.GenerateScenesEnum(); };
            EditorApplication.playModeStateChanged += LoadInitScene;
        }

        //
        public static EditorBuildSettingsScene InitScene
        {
            get { return EditorBuildSettings.scenes[0]; }
        }

        //
        private static string PreviousScene
        {
            get { return EditorPrefs.GetString(PreviousScenePrefsKey, EditorSceneManager.GetActiveScene().path); }
            set { EditorPrefs.SetString(PreviousScenePrefsKey, value); }
        }

        //
        private void GenerateScenesEnum()
        {

            if (enabled == false) { return; }
                
            List<string> identifiers = new List<string>();
            List<string> names = new List<string>();

            var scenes = new List<EditorBuildSettingsScene>(EditorBuildSettings.scenes);
            scenes.Sort((scene1, scene2) => scene1.CompareTo(scene2));

            foreach (var scene in scenes)
            {
                string sceneName = Path.GetFileNameWithoutExtension(scene.path);
                identifiers.Add(sceneName.ToPascalCase().RemoveUnderscorePrefix());
                names.Add(sceneName);
            }

            EnumCreationParameters parameters = new EnumCreationParameters()
            {
                Path = ScriptFileHelpers.GetScriptPath(nameof(SceneIdentifier), ScriptFileDirectory),
                Namespace = nameof(Utilities),
                Description = EnumDescription,
                Identifiers = identifiers,
                StringValues = names
            };

            EnumGenerationHelpers.CreateEnumScriptAsset(parameters);
        }


        //
        static void LoadInitScene(PlayModeStateChange state)
        {

            if (ToolScriptableObject.loadInitScene == false) { return; }

            if (EditorBuildSettings.scenes.Length == 0) { return; }

            // Load init scene
            if (!EditorApplication.isPlaying && EditorApplication.isPlayingOrWillChangePlaymode)
            {
                Scene scene = EditorSceneManager.GetActiveScene();
                PreviousScene = scene.path;
                EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

                string initScenePath = InitScene.path;
                EditorSceneManager.OpenScene(initScenePath);
            }

            // Load previously loaded scene
            if (!EditorApplication.isPlaying && !EditorApplication.isPlayingOrWillChangePlaymode)
            {
                EditorSceneManager.OpenScene(PreviousScene);
            }

        }

 

    }
}
