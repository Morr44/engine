﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using Utilities.Helpers;


namespace Utilities
{
    /// <summary>
    /// Controls scenes loading/unloading process
    /// </summary>
    public class SceneLoadingController : MonoBehaviourSingleton<SceneLoadingController>
    {

        public System.Action<SceneIdentifier> OnLoadingBegin = delegate { };
        public System.Action<SceneIdentifier> OnLoadingComplete = delegate { };

        //
        public bool IsLoading { get; private set; }

        //
        public float Progress { get; private set; }

        //
        public void LoadScene(SceneIdentifier sceneId)
        {
            StartCoroutine(LoadSceneCoroutine(sceneId, true));
        }

        //
        public void LoadScene(SceneIdentifier sceneId, bool unloadCurrent)
        {
            StartCoroutine(LoadSceneCoroutine(sceneId, unloadCurrent));
        }

        //
        private IEnumerator LoadSceneCoroutine(SceneIdentifier sceneId, bool unload)
        {
            // Wait if any scene is being loaded
            yield return new WaitUntil(() => IsLoading == false);

            OnLoadingBegin(sceneId);
            IsLoading = true;

            // Cache active scene
            Scene prevScene = SceneManager.GetActiveScene();

            // Start loading
            string sceneName = Path.GetFileNameWithoutExtension(sceneId.GetStringValue());
            AsyncOperation sceneLoading = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            sceneLoading.allowSceneActivation = true;

            // Update progress value
            while (!sceneLoading.isDone)
            {
                Progress = sceneLoading.progress;
                yield return new WaitForEndOfFrame();
            }

            // Unload previous scene
            if (unload)
            {
                AsyncOperation sceneUnloading = SceneManager.UnloadSceneAsync(prevScene);
                yield return new WaitUntil(() => !sceneUnloading.isDone);
            }

            IsLoading = false;
            OnLoadingComplete(sceneId);

        }


        #region UnityEvents
        //
        protected override void Awake()
        {
            base.Awake();
            DontDestroyOnLoad(this.gameObject);
        }
        #endregion

    }
}