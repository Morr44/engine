using UnityEngine;
using UnityEditor;

// Template description
// 0 - Full namespace name
// 1 - Enum description
// 2 - Enum type name
// 3 - Enum values

namespace Utilities
{
	/// <summary>
	/// Generated scenes enum
	/// </summary>
	public enum SceneIdentifier
	{
		[EnumStringValue("03_SampleScene")]
		SampleScene,
		[EnumStringValue("02_Loading")]
		Loading,
		[EnumStringValue("01_Menu")]
		Menu,

	}

}
