﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities.EditorHelpers;
using System.Linq;

namespace Utilities.EditorTools
{
    /// <summary>
    /// 
    /// </summary>
    public class ScriptingDefinesManager : Tool<ScriptingDefinesManager>
    {

        [SerializeField] List<ScriptDefineEntry> defineEntries;
        [SerializeField, HideInInspector] List<string> localDefines;

        //
        public void PushDefines()
        {
            if (!enabled) { return; }

            List<string> newDefines = ScriptingDefineHelpers.GetDefinesList();
            FilterOutLocalDefines(newDefines);

            // Select enabled local defines
            localDefines = defineEntries.
                Where((entry) => entry.Enabled).
                Select((entry) => entry.Identifier).ToList();

            // Add current local defines
            foreach (var localDefine in localDefines)
                newDefines.Add(localDefine);

            ScriptingDefineHelpers.SetDefinesList(newDefines);

        }
        
        //
        public void FilterOutLocalDefines(List<string> defines)
        {
            foreach (var localDefine in localDefines)
            {
                int index = defines.IndexOf(localDefine);
                if (index > 0) { defines.RemoveAt(index); }
            }
        }

        //
        public void RefreshValues()
        {
            var currentDefines = ScriptingDefineHelpers.GetDefinesList();

            foreach (var entry in defineEntries)
            {
                entry.Enabled = currentDefines.Contains(entry.Identifier);
            }
        }



        #region NestedTypes

        /// <summary>
        /// 
        /// </summary>
        [System.Serializable]
        public class ScriptDefineEntry
        {
            public string Identifier;
            public bool Enabled;
        }

        #endregion

    }
}