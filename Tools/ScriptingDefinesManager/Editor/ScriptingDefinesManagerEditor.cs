﻿using System.IO;
using UnityEditor;
using Malee.Editor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Utilities.EditorTools
{

    [CustomEditor(typeof(ScriptingDefinesManager))]
    public class ScriptingDefinesManagerEditor : ToolEditor
    {

        private SerializedProperty definesProperty;
        private ReorderableList definesList;


        //
        protected override void DisplayToolSpecificSettings()
        {
            definesList.DoLayoutList();
        }

        //
        protected override void ApplySettings()
        {
            base.ApplySettings();
            ScriptingDefinesManager.ToolScriptableObject.PushDefines();
        }


        #region MenuItems

        //
        [MenuItem(ToolsMenuPath + "Scripting Defines Manager")]
        public static void SelectToolScriptableObject()
        {
            Selection.activeObject = ScriptingDefinesManager.ToolScriptableObject;
        }

        #endregion


        #region UnityEvents

        //
        protected override void OnEnable()
        {
            base.OnEnable();
            definesProperty = serializedObject.FindProperty("defineEntries");
            definesList = new ReorderableList(definesProperty);
        }

        //
        public override void OnInspectorGUI()
        {
            ScriptingDefinesManager.ToolScriptableObject.RefreshValues();
            base.OnInspectorGUI();
        }

        #endregion

    }
}