﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


namespace Utilities.EditorTools
{
    /// <summary>
    /// 
    /// </summary>
    [CustomPropertyDrawer(typeof(ScriptingDefinesManager.ScriptDefineEntry))]
    public class UserInfoDrawer : PropertyDrawer
    {

        private int toggleWidth = 20;
        private int toggleMargin = 5;
        private int verticalSpacing = 4;

        //
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight + verticalSpacing;
        }

        //
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);


            Rect identifierRect = new Rect(position.x, position.y, position.width - toggleWidth - toggleMargin * 2, 16);
            Rect enabledRect = new Rect(position.x + position.width - toggleWidth - toggleMargin, position.y, toggleWidth, 16);

            SerializedProperty identifierProperty = property.FindPropertyRelative("Identifier");
            SerializedProperty enabledProperty = property.FindPropertyRelative("Enabled");

            identifierProperty.stringValue = EditorGUI.DelayedTextField(identifierRect, identifierProperty.stringValue);
            enabledProperty.boolValue = EditorGUI.Toggle(enabledRect, enabledProperty.boolValue);

            EditorGUI.EndProperty();
        }


    }
}