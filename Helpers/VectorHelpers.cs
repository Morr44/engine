﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;


namespace Utilities.Helpers
{
	/// <summary>
	/// Vector3 operations
	/// </summary>
	public static class VectorHelpers 
	{

        /// <summary>
        /// Clamps distance from the target point to the specified min value
        /// </summary>
        public static Vector3 ClampDistance(this Vector3 point, Vector3 targetPoint, float clampDistance)
        {
            Vector3 targetVector = targetPoint - point;
            float distance = targetVector.magnitude;

            if (distance > clampDistance)
                return targetPoint - (targetVector.normalized * clampDistance);

            return point;
        }

	}
}