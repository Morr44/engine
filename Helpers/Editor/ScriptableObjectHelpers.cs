﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;


namespace Utilities.EditorHelpers
{

    /// <summary>
    /// 
    /// </summary>
    public class ScriptableObjectHelpers
    {

        /// <summary>
        ///	Create, name and place unique new ScriptableObject asset files.
        /// </summary>
        public static T CreateScriptableObjectAsset<T>(string directory) where T : ScriptableObject
        {
            T asset = ScriptableObject.CreateInstance<T>();
            string path = AssetDatabase.GenerateUniqueAssetPath(directory + "/" + typeof(T).Name + ".asset");
            AssetDatabase.CreateAsset(asset, path);

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            //EditorUtility.FocusProjectWindow();
            //Selection.activeObject = asset;

            return asset;
        }

    }
}