﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Utilities.EditorHelpers
{
    /// <summary>
    /// Helper methods performing operations on project defines
    /// </summary>
    public class ScriptingDefineHelpers
    {

        /// <summary>
        /// Retrive project defines from project settings
        /// </summary>
        /// <returns>Defines as list of strings</returns>
        public static List<string> GetDefinesList()
        {
            var group = GetBuildTargetGroup();
            string defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(group);
            return new List<string>(defines.Split(';'));
        }


        /// <summary>
        /// Store defines in project settings
        /// </summary>
        /// <param name="defines">Defines list</param>
        public static void SetDefinesList(List<string> defines)
        {
            var group = GetBuildTargetGroup();
            PlayerSettings.SetScriptingDefineSymbolsForGroup(group, string.Join(";", defines));
        }


        /// <summary>
        /// Retrive target build group from project settings
        /// </summary>
        /// <returns>Build target group</returns>
        private static BuildTargetGroup GetBuildTargetGroup()
        {
            var targetPlatform = EditorUserBuildSettings.activeBuildTarget;
            return BuildPipeline.GetBuildTargetGroup(targetPlatform);
        }

    }

}