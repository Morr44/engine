﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;
using System.Text;
using UnityEditor;
using System.IO;

namespace Utilities.EditorHelpers
{
    /// <summary>
    /// 
    /// </summary>
    public struct EnumCreationParameters
    {
        public string Path;
        public string Namespace;
        public string Description;
        public List<string> Identifiers;
        public List<int> Values;
        public List<string> StringValues;
    }


    /// <summary>
    /// Common operations on name strings
    /// </summary>
    public static class EnumGenerationHelpers
    {

        /// <summary>
        /// 
        /// </summary>
        public static void CreateEnumScriptAsset(EnumCreationParameters parameters)
        {
            string currentDirectory = ScriptFileHelpers.GetScriptDirectory(nameof(EnumGenerationHelpers));
            TextAsset enumTemplate = AssetDatabase.LoadAssetAtPath(currentDirectory + "\\ScriptTemplates\\EnumTemplate.txt", typeof(TextAsset)) as TextAsset;
            StringBuilder stringBuilder = new StringBuilder();

            AppendValues(stringBuilder, parameters);

            string typeName = Path.GetFileNameWithoutExtension(parameters.Path);

            string enumScript = string.Format(
                enumTemplate.text,
                parameters.Namespace,
                parameters.Description,
                typeName,
                stringBuilder
                );

            ScriptFileHelpers.SaveScript(parameters.Path, enumScript);
        }


        /// <summary>
        /// 
        /// </summary>
        private static void AppendValues(StringBuilder stringBuilder, EnumCreationParameters parameters)
        {
            for (int i = 0; i < parameters.Identifiers.Count; i++)
            {
                string value = parameters.Values == null ? string.Empty : " = " + parameters.Values[i].ToString();
                if (parameters.StringValues != null)
                    stringBuilder.AppendLine("\t\t[" + typeof(EnumStringValue).Name + "(\"" + parameters.StringValues[i] + "\")]");
                stringBuilder.AppendLine("\t\t" + parameters.Identifiers[i] + value + ",");
            }

        }
    }

}