﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Utilities.EditorHelpers
{
    /// <summary>
    /// Logs & Console operations
    /// </summary>
    public class ConsoleHelpers
    {
        /// <summary>
        /// Clear debug console
        /// </summary>
        public static void ClearConsole()
        {
            var assembly = Assembly.GetAssembly(typeof(SceneView));
            var type = assembly.GetType("UnityEditor.LogEntries");
            var method = type.GetMethod("Clear");
            method.Invoke(new object(), null);
        }
    }
}
