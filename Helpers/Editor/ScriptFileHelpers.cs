﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Utilities.EditorHelpers
{
    /// <summary>
    /// Common script operations (meta programming)
    /// </summary>
    public class ScriptFileHelpers
    {

        /// <summary>
        /// Script file path
        /// </summary>
        /// <param name="scriptFileName">Name of the script file (without extension)</param>
        /// <param name="fallbackDirectory">If script was not found then (fallbackDirectory + scriptFileName + ".cs") path will be returned</param>
        /// <returns></returns>
        public static string GetScriptPath(string scriptFileName, string fallbackDirectory = null)
        {
            string[] assets = AssetDatabase.FindAssets(scriptFileName);
            foreach (string guid in assets)
            {
                string path = AssetDatabase.GUIDToAssetPath(guid);
                if(AssetDatabase.GetMainAssetTypeAtPath(path) == typeof(MonoScript))
                    if (Path.GetFileNameWithoutExtension(path).Equals(scriptFileName))
                        return path;
            }

            if(fallbackDirectory != null) { return fallbackDirectory + scriptFileName + ".cs"; }
            return string.Empty;
        }



        /// <summary>
        /// Scipt file directory
        /// </summary>
        /// <param name="scriptFileName">Name of the script file (with extension)</param>
        /// <returns>Script file directory</returns>
        public static string GetScriptDirectory(string scriptFileName)
        {
            string path = GetScriptPath(scriptFileName);
            return Path.GetDirectoryName(path);
        }


        /// <summary>
        /// Save script under given path
        /// </summary>
        /// <param name="path">Absolute file path</param>
        public static void SaveScript(string path, string script)
        {
            TextAsset scriptAsset = AssetDatabase.LoadAssetAtPath<TextAsset>(path);
            if(scriptAsset != null)
            {
                if (script.Equals(scriptAsset.text))
                {
                    return;
                }
            }
            else
            {
                scriptAsset = new TextAsset();
                AssetDatabase.CreateAsset(scriptAsset, path);
            }

            File.WriteAllText(path, script);
            AssetDatabase.ImportAsset(path);
            //AssetDatabase.SaveAssets(); 
        }




    }
}