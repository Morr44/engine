﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;
using Utilities.Helpers;

namespace Utilities.Helpers
{
	/// <summary>
	/// Used as boolean flag controlled by multiple objects
	/// </summary>
    [System.Serializable]
	public class CounterLock 
	{
        private int counter;
        private bool locked;

        /// <summary>
        /// Returns true if counter value is greater than 0
        /// </summary>
        public bool Locked
        {
            get { return locked; }
        }

        //
        public static CounterLock operator ++(CounterLock counterLock)
        {
            counterLock.counter++;
            counterLock.locked = true;
            return counterLock;
        }

        //
        public static CounterLock operator --(CounterLock counterLock)
        {
            counterLock.counter--;
            if(counterLock.counter <= 0)
            {
                counterLock.locked = false;
            }
            return counterLock;
        }

    }
}