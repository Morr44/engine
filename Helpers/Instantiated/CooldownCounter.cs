﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;
using Utilities.Helpers;

namespace Utilities.Helpers
{
	/// <summary>
	/// Cooldown calculation helper
	/// </summary>
    [System.Serializable]
	public class CooldownCounter 
	{
        [SerializeField] private float defaultCooldownDuration;
        private float cooldownEndTime;


        //
        public CooldownCounter() {}

        //
        public CooldownCounter(float cooldownDuration)
        {
            this.defaultCooldownDuration = cooldownDuration;
        }

        //
        public bool OnCooldown
        {
            get { return Time.time < cooldownEndTime; }
        }

        //
        public void Trigger()
        {
            float newCooldownEndTime = Time.time + defaultCooldownDuration;
            cooldownEndTime = Mathf.Max(cooldownEndTime, newCooldownEndTime);
        }

        //
        public void Trigger(float cooldoownDuration)
        {
            float newCooldownEndTime = Time.time + cooldoownDuration;
            cooldownEndTime = Mathf.Max(cooldownEndTime, newCooldownEndTime);
        }
    }
}