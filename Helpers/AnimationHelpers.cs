﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;


namespace Utilities.Helpers
{
	/// <summary>
	/// Animation related utilities
	/// </summary>
	public static class AnimationUtilities
	{

        /// <summary>
        /// Returns a new inverted curve
        /// </summary>
        public static AnimationCurve GetInvertedCurve(this AnimationCurve curve)
        {
            AnimationCurve invertedCurve = new AnimationCurve();
            for (int i = 0; i < curve.length; i++)
            {
                Keyframe inverseKey = new Keyframe(curve.keys[i].value, curve.keys[i].time);
                invertedCurve.AddKey(inverseKey);
            }

            return invertedCurve;
        }


	}
}