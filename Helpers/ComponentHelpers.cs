﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;


namespace Utilities
{
	/// <summary>
	/// Conponent utilities
	/// </summary>
	public static class ComponentHelpers 
	{

        //
        public static ComponentValues GetComponentCopy(this Component component)
        {
            return new ComponentValues(component);
        }


        #region NestedTypes

        /// <summary>
        /// 
        /// </summary>
        public class ComponentValues
        {
            private System.Type type;
            private System.Reflection.FieldInfo[] fields;

            //
            public ComponentValues(Component component)
            {
                type = component.GetType();
                fields = type.GetFields();
            }

        }


        #endregion


    }
}