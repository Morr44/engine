﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;


namespace Utilities.Helpers
{
	/// <summary>
	/// Float operations
	/// </summary>
	public static class FloatHelpers 
	{

        /// <summary>
        /// 
        /// </summary>
        public static float ApproachTergetValue(float value, float targetValue, float speed)
        {
            // Equals
            if(value == targetValue)
            {
                return value;
            }

            // Increment or decrement value
            if(value < targetValue)
            {
                value += speed;
                if (value > targetValue)
                {
                    value = targetValue;
                }
            }
            else
            {
                value -= speed;
                if (value < targetValue)
                {
                    value = targetValue;
                }
            }
            return value;
        }

	}
}