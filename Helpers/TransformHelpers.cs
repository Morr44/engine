﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;


namespace Utilities.Helpers
{
	/// <summary>
	/// 
	/// </summary>
	public static class TransformHelpers 
	{

        public static void SetLocalPositionX(this Transform transform, float x)
        {
            Vector3 tmp = transform.localPosition;
            tmp.x = x;
            transform.localPosition = tmp;
        }

        //
        public static void SetLocalPositionY(this Transform transform, float y)
        {
            Vector3 tmp = transform.localPosition;
            tmp.y = y;
            transform.localPosition = tmp;
        }

        //
        public static void SetLocalPositionZ(this Transform transform, float z)
        {
            Vector3 tmp = transform.localPosition;
            tmp.z = z;
            transform.localPosition = tmp;
        }



        //
        public static void SetPositionX(this Transform transform, float x)
        {
            Vector3 tmp = transform.position;
            tmp.x = x;
            transform.position = tmp;
        }

        //
        public static void SetPositionY(this Transform transform, float y)
        {
            Vector3 tmp = transform.position;
            tmp.y = y;
            transform.position = tmp;
        }

        //
        public static void SetPositionZ(this Transform transform, float z)
        {
            Vector3 tmp = transform.position;
            tmp.z = z;
            transform.position = tmp;
        }




        //
        public static void SetLocalScaleX(this Transform transform, float x)
        {
            Vector3 tmp = transform.localScale;
            tmp.x = x;
            transform.localScale = tmp;
        }

        //
        public static void SetLocalScaleY(this Transform transform, float y)
        {
            Vector3 tmp = transform.localScale;
            tmp.y = y;
            transform.localScale = tmp;
        }

        //
        public static void SetLocalScaleZ(this Transform transform, float z)
        {
            Vector3 tmp = transform.localScale;
            tmp.z = z;
            transform.localScale = tmp;
        }

        //
        public static Matrix4x4 LerpMatrix(Matrix4x4 from, Matrix4x4 to, float t)
        {
            Matrix4x4 tmp = new Matrix4x4();
            for (int i = 0; i < 16; i++)
                tmp[i] = Mathf.Lerp(from[i], to[i], t);
            return tmp;
        }



    }
}