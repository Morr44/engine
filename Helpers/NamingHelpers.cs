﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEditor;

namespace Utilities.Helpers
{
    /// <summary>
    /// Common string operations
    /// </summary>
    public static class NamingHelpers
    {
        /// <summary>
        /// Retrives number from the asset name
        /// </summary>
        public static int RetriveAssetNameNumber(string path)
        {
            string assetName = Path.GetFileNameWithoutExtension(path);
            string number = new string(assetName.Where(c => char.IsDigit(c)).ToArray());

            int parseResult;
            if (int.TryParse(number, out parseResult))
                return parseResult;
            else
                return int.MaxValue;
        }

        /// <summary>
        /// Converts string to pascal notation
        /// </summary>
        public static string ToPascalCase(this string input)
        {
            string output = input.Trim();
            output = Regex.Replace(output, @"\s+\w", m => m.Value.ToUpper().TrimStart());
            return output;
        }

        /// <summary>
        /// Removes the first found underscore and all characters before it
        /// </summary>
        public static string RemoveUnderscorePrefix(this string input)
        {
            int underscoreIndex = input.IndexOf('_');
            if(underscoreIndex > 0)
            {
                return input.Substring(underscoreIndex + 1);
            }
            else
            {
                return input;
            }
        }

        /// <summary>
        /// Turns auto implemented property backing field name into string that could be displayed in inspector
        /// </summary>
        public static string NicifyBackingFieldName(string name)
        {
            Regex filter = new Regex(@"<(.+?)>");
            Match match = filter.Match(name);
            if (match.Success == true)
            {
                return ObjectNames.NicifyVariableName(match.Groups[1].Value);
            }
            return name;
        }

    }

}
