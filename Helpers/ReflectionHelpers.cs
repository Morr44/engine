﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Utilities.Helpers
{
    // Common operations on Enum/Class types
    public static class ReflectionHelpers
    {
        private static BindingFlags GetFieldsBindingFlags = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static;

        public static IEnumerable<Type> GetEnumsWithAttribute(Type attribute)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            foreach (Type type in assembly.GetTypes())
            {
                if (type.IsEnum && type.GetCustomAttributes(attribute, true).Length > 0)
                    yield return type;
            }
        }

        public static List<FieldInfo> GetFieldsWithAttribute(Type type, Type attribute)
        {
            if (type.IsSubclassOf(typeof(ExtendedMonoBehaviour)) == false)
            {
                Debug.LogError($"Lookup type must be derived from {typeof(ExtendedMonoBehaviour).Name}");
            }

            bool includePublic = true;
            List<FieldInfo> componentFields = new List<FieldInfo>();
            List<FieldInfo> tmpComponentFields;
            while (type != typeof(ExtendedMonoBehaviour))
            {
                tmpComponentFields = new List<FieldInfo>(type.GetFields(GetFieldsBindingFlags));
                tmpComponentFields.RemoveAll((fieldInfo) => Attribute.IsDefined(fieldInfo, attribute) == false);

                if (includePublic == false)
                {
                    tmpComponentFields.RemoveAll((fieldInfo) => fieldInfo.IsPublic == true);
                }

                includePublic = false;
                componentFields.AddRange(tmpComponentFields);
                type = type.BaseType;
            }

            return componentFields;
        }

        public static bool IsGenericList(this Type type)
        {
            return (type.IsGenericType && (type.GetGenericTypeDefinition() == typeof(List<>)));
        }

        public static IList CreateList(Type elementType, IList array)
        {
            Type genericListType = typeof(List<>).MakeGenericType(elementType);
            IList list =(IList)Activator.CreateInstance(genericListType);
            for(int i = 0; i < array.Count; i++)
            {
                list.Add(array[i]);
            }
            return list;
        }


        public static bool HasAnyOfAttributes(this SerializedProperty property, params Type[] attributes)
        {
            Type type = property.serializedObject.targetObject.GetType();
            while (type != typeof(ExtendedMonoBehaviour))
            {
                FieldInfo fieldInfo = type.GetField(property.propertyPath, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                if (fieldInfo != null)
                {
                    for (int i = 0; i < attributes.Length; i++)
                    {
                        if (fieldInfo.GetCustomAttribute(attributes[i]) != null)
                            return true;
                    }
                }
                type = type.BaseType;
            }
            return false;
        }
        

    }
}
