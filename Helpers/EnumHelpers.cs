﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;
using System;
using System.Reflection;


namespace Utilities.Helpers
{
	/// <summary>
	/// 
	/// </summary>
	public static class EnumHelpers 
	{

        /// <summary>
        /// Will get the string value for a given enums value, this will
        /// only work if you assign the StringValue attribute to
        /// the items in your enum.
        /// </summary>
        /// <param name="value"></param>
        /// <returns>Value of StringValue attribute; Empty string if value is not defined</returns>
        public static string GetStringValue(this System.Enum value)
        {
            Type type = value.GetType();
            if (!Enum.IsDefined(type, value))
                return string.Empty;

            FieldInfo fieldInfo = type.GetField(Enum.GetName(type, value));
            EnumStringValue[] attributes = fieldInfo.GetCustomAttributes(typeof(EnumStringValue), false) as EnumStringValue[];
            return attributes.Length > 0 ? attributes[0].Value : null;
        }

    }
}