﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Utilities.Helpers
{
    /// <summary>
    /// Common collection operations
    /// </summary>
    public static class CollectionHelpers
    {
        /// <summary>
        /// Swaps two elements of the list
        /// </summary>
        public static void SwapElements<T>(this List<T> list, T firstElement, T secondElement)
        {
            int firstIndex = list.IndexOf(firstElement);
            int secondIndex = list.IndexOf(secondElement);
            list[firstIndex] = secondElement;
            list[secondIndex] = firstElement;
        }

        /// <summary>
        /// Returns the last element of an array
        /// </summary>
        public static T GetLastElement<T>(this T[] array)
        {
            return array[array.Length - 1];
        }

        /// <summary>
        /// Sets collection element to null and returns an object to which reference was pointing
        /// </summary>
        public static T NullifyElement<T>(this IList<T> collection, T element) where T : class
        {
            for(int i = 0; i < collection.Count; i++)
            {
                if(collection[i] == element)
                {
                    T tmp = element;
                    collection[i] = null;
                    return element;
                }
            }
            return null;
        }

        /// <summary>
        /// Replaces first null with reference to the element
        /// </summary>
        public static bool ReplaceFirstEmpty<T>(this T[] array, T element) where T : class
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == null)
                {
                    array[i] = element;
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Returns random element of an array
        /// </summary>
        public static T GetRandomElement<T>(this T[] array)
        {
            int randomIndex = UnityEngine.Random.Range(0, array.Length);
            return array[randomIndex];
        }

    }
}