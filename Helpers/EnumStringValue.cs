﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This attribute is used to represent a string value
/// for a value in an enum.
/// </summary>
public class EnumStringValue : Attribute
{

    /// <summary>
    /// Holds the stringvalue for a value in an enum.
    /// </summary>
    public string Value { get; protected set; }


    /// <summary>
    /// Constructor used to init a StringValue Attribute
    /// </summary>
    /// <param name="value"></param>
    public EnumStringValue(string value)
    {
        this.Value = value;
    }

}